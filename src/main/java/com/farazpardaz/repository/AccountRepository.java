package com.farazpardaz.repository;

import com.farazpardaz.beans.Account;
import jdk.nashorn.internal.ir.LiteralNode;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
    public Account findAccountById(Long id);
    public Account findAccountByAccountNO(String accountNO);
    public List<Account> findAll();

}
