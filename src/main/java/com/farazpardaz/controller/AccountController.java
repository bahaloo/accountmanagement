package com.farazpardaz.controller;

import com.farazpardaz.beans.Account;
import com.farazpardaz.service.AccountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(value = "/api")
public class AccountController {

    @Autowired
    private AccountingService accountingService;

    @RequestMapping(method = RequestMethod.GET, value = "/accounts", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Account>> getAccounts() {
        Collection<Account> accounts = accountingService.getAllAccounts();
        return new ResponseEntity<Collection<Account>>(accounts, HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/accounts/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> getGreeting(@PathVariable("id") Long id) {
        Account account = accountingService.findAccountById(id);
        if(account == null) {
            return new ResponseEntity<Account>(HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<Account>(account, HttpStatus.OK);
    }

    @RequestMapping(value = "/create/account", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> createAccount(@RequestBody Account account) {
        Account savedAccount = accountingService.createAccount(account);
        return new ResponseEntity<Account>(savedAccount, HttpStatus.CREATED);
    }

    @RequestMapping(value = "/update/account/{id}", method = RequestMethod.PUT, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Account> updateGreeting(@PathVariable("id") Long id, @RequestBody Account account) {
        Account updatedAccount = null;
        if (account != null && id == account.getId()) {
            updatedAccount = accountingService.updateAccount(account);
        }
        if(updatedAccount == null) {
            return new ResponseEntity<Account>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Account>(updatedAccount, HttpStatus.OK);
    }


    @RequestMapping(value = "/delete/account/{id}", method = RequestMethod.DELETE)
    public ResponseEntity<Account> deleteAccount(@PathVariable("id") Long id) {
        accountingService.deleteAccount(id);
        return new ResponseEntity<Account>(HttpStatus.NO_CONTENT);
    }
}
