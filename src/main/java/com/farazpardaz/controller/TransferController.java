package com.farazpardaz.controller;

import com.farazpardaz.beans.Account;
import com.farazpardaz.enums.CCY;
import com.farazpardaz.service.AccountingService;
import com.farazpardaz.beans.Account;
import com.farazpardaz.enums.CCY;
import com.farazpardaz.service.AccountingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/api")
public class TransferController
{
    @Autowired
    private AccountingService accountingService;

    @Transactional
    @RequestMapping(method = RequestMethod.GET,value = "/transfer/{srcAccountNO}/{destAccountNO}/{amount}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<HttpStatus> transferMoney(@PathVariable ("srcAccountNO") String srcAccountNO,
                                                    @PathVariable ("destAccountNO") String destAccountNO,
                                                    @PathVariable ("amount") int amount) {


        Account srcAcct = accountingService.findAccountByAccountNO(srcAccountNO);
        Account destAcct = accountingService.findAccountByAccountNO(destAccountNO);

        CCY srcCCY = srcAcct.getCurrencyType();
        CCY destCCY = destAcct.getCurrencyType();



        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<Integer> response = restTemplate.getForEntity("http://localhost:8081/exchange/" + srcCCY + "/" + destCCY + "/" + amount,
                Integer.class);

        srcAcct.setBalance(srcAcct.getBalance() - amount);
        accountingService.updateAccount(srcAcct);

        destAcct.setBalance(destAcct.getBalance() + amount);
        accountingService.updateAccount(destAcct);

        return new ResponseEntity<>(HttpStatus.OK);
    }
}
