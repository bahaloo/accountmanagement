package com.farazpardaz.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api")
public class TestController {
    @RequestMapping(method = RequestMethod.GET, value = "/test")
    public ResponseEntity<Integer> test() {
        return new ResponseEntity<Integer>(1234, HttpStatus.OK);
    }
}
