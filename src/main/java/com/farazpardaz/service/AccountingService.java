package com.farazpardaz.service;

import com.farazpardaz.beans.Account;
import org.springframework.stereotype.Service;

import java.util.List;

public interface AccountingService{
    public Account findAccountById(Long id);
    public Account findAccountByAccountNO(String accountNO);
    public Account createAccount(Account account);
    public Account updateAccount(Account account);
    public void deleteAccount(String accountNO);
    public void deleteAccount(Long id);
    public List<Account> getAllAccounts();
}