package com.farazpardaz.service;

import com.farazpardaz.beans.Account;
import com.farazpardaz.beans.Account;
import com.farazpardaz.repository.AccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AccountingServiceImpl implements AccountingService{

    @Autowired
    AccountRepository accountRepository;

    @Override
    public Account findAccountByAccountNO(String accountNO){
        return accountRepository.findAccountByAccountNO(accountNO);
    }

    @Override
    public Account findAccountById(Long id) {
        return accountRepository.findAccountById(id);
    }

    @Override
    public Account createAccount(Account account) {
        if (account.getId() != null){
            return null;
        }

        Account accountRes = accountRepository.save(account);
        return accountRes;
    }

    @Override
    public Account updateAccount(Account account) {
        Account acct = findAccountById(account.getId());
        if (acct.getId() == null){
            return null;
        }
        acct = accountRepository.save(account);
        return acct;
    }

    @Override
    public void deleteAccount(String accountNO) {
        Account acct = findAccountByAccountNO(accountNO);
        accountRepository.delete(acct);
    }

    @Override
    public void deleteAccount(Long id) {
        Account acct = findAccountById(id);
        accountRepository.delete(acct);
    }

    @Override
    public List<Account> getAllAccounts() {
        return accountRepository.findAll();
    }
}
