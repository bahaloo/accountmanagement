package com.farazpardaz.beans;

import com.farazpardaz.enums.CCY;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;
    private String name;
    private String accountNO;
    private int balance;
    private CCY currencyType;

    public Account() {
    }

    public Account(String name, String accountNO, int balance, CCY currencyType) {
        this.name = name;
        this.accountNO = accountNO;
        this.balance = balance;
        this.currencyType = currencyType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccountNO() {
        return accountNO;
    }

    public void setAccountNO(String accountNO) {
        this.accountNO = accountNO;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public CCY getCurrencyType() {
        return currencyType;
    }

    public void setCurrencyType(CCY currencyType) {
        this.currencyType = currencyType;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
